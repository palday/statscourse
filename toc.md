# Crash Course Statistics

0. Probability (Unconditional)
1. Descriptive Statistics
  - distributions
    - binomial
    - Gaussian 
  - Mean, Median, Mode
  - Standard Deviation, Standard Error of Mean
  - F-test, t-test?
2. Inferential Statistics
  - Mean of measurements (e.g. in high school science)
  - Mean as a Model
  - Residual Error
3. The (General) Linear Model
4. Frequentist Approaches and Null-Hypothesis Testing
  - Types of Error
    - Alpha and Beta
    - Medical Testing
  - Assumptions of Normality
    - parametric testing
    - relationship to theoretical distributions
  - p-value
  - ANOVA
  - Repeated Measures
  - Non parametric methods
    - Permutation tests
    - Bootstrapping
    - Jackknife
5. Dealing with Error
  - Family-Wise Error
  - FDR
  - Bonferoni
6. Bayesian Approaches
  - Conditional Probability  
    - Monty Hall Problem
    - Bayes Theorem
  - Degree of Confidence
    - LR
    - EER
7. Statistical and Practical Significance
  - Effect Size
    - Absolute
    - Standarized
  - Probability and Predictions, Sigificance and Explanations
A. Mathematical Background
  - Arithmetic Laws
  - Set Theory and Boolean Algebra
  - Logs and Exponents
  - Trigonometric Functions
